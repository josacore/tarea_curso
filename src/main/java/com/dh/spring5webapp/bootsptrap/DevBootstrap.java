/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.bootsptrap;

import java.util.*;

import com.dh.spring5webapp.model.*;
import com.dh.spring5webapp.repositories.*;
import com.dh.spring5webapp.services.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryService categoryRepository;
    private SubCategoryService subCategoryRepository;
    private ItemService itemRepository;
    private EmployeeService employeeRepository;
    private PositionService positionRepository;
    private ContractService contractRepository;
    private CommentService commentRepository;

    public DevBootstrap(CategoryService categoryRepository, SubCategoryService subCategoryRepository,
          ItemService itemRepository, EmployeeService employeeRepository, PositionService positionRepository,
          ContractService contractRepository,CommentService commentRepository) {
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
        this.contractRepository = contractRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();
    }

    private void initData() {

        Category eppCategory = new Category();
        eppCategory.setName("Personal Protection Equipment");
        eppCategory.setCode("EPP");

        Category resourceCategory = new Category();
        resourceCategory.setCode("EES");
        resourceCategory.setName("EMERGENCY EXIT SIGNS");

        Category resourceCategory3 = new Category();
        resourceCategory3.setCode("POR");
        resourceCategory3.setName("PORTABLE");

        Category resourceCategory4 = new Category();
        resourceCategory4.setCode("ANY");
        resourceCategory4.setName("ANYTHING");

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        SubCategory safetySubCategory3 = new SubCategory();
        safetySubCategory3.setCategory(eppCategory);
        safetySubCategory3.setCode("EYE");
        safetySubCategory3.setName("EYEWEAR");

        SubCategory safetySubCategory4 = new SubCategory();
        safetySubCategory4.setCategory(eppCategory);
        safetySubCategory4.setCode("GLV");
        safetySubCategory4.setName("GLOVES");

        SubCategory safetySubCategory5 = new SubCategory();
        safetySubCategory5.setCategory(resourceCategory3);
        safetySubCategory5.setCode("EXT");
        safetySubCategory5.setName("EXTINGUSHER");

        SubCategory safetySubCategory6 = new SubCategory();
        safetySubCategory6.setCategory(resourceCategory3);
        safetySubCategory6.setCode("FBL");
        safetySubCategory6.setName("FIRE BLANKERS");

        SubCategory safetySubCategory7 = new SubCategory();
        safetySubCategory7.setCategory(resourceCategory3);
        safetySubCategory7.setCode("WAL");
        safetySubCategory7.setName("WATHER ALARMS");

        SubCategory safetySubCategory8 = new SubCategory();
        safetySubCategory8.setCategory(resourceCategory);
        safetySubCategory8.setCode("LEXR");
        safetySubCategory8.setName("Lithonia EXR LED EL M6 ");

        SubCategory safetySubCategory9 = new SubCategory();
        safetySubCategory9.setCategory(resourceCategory4);
        safetySubCategory9.setCode("HSER");
        safetySubCategory9.setName("Half-Mask Paint & Pesticide Respirator, Safety Works ");

        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");

        eppCategory.getSubCategory().add(safetySubCategory);
        eppCategory.getSubCategory().add(safetySubCategory3);
        eppCategory.getSubCategory().add(safetySubCategory4);
        resourceCategory.getSubCategory().add(safetySubCategory8);
        resourceCategory3.getSubCategory().add(safetySubCategory5);
        resourceCategory3.getSubCategory().add(safetySubCategory6);
        resourceCategory3.getSubCategory().add(safetySubCategory7);
        resourceCategory4.getSubCategory().add(safetySubCategory9);
        resourceCategory.getSubCategory().add(rawMaterialSubCategory);
        categoryRepository.save(eppCategory);
        categoryRepository.save(resourceCategory);
        categoryRepository.save(resourceCategory3);
        categoryRepository.save(resourceCategory4);
        subCategoryRepository.save(safetySubCategory);
        subCategoryRepository.save(safetySubCategory3);
        subCategoryRepository.save(safetySubCategory4);
        subCategoryRepository.save(safetySubCategory5);
        subCategoryRepository.save(safetySubCategory6);
        subCategoryRepository.save(safetySubCategory7);
        subCategoryRepository.save(safetySubCategory8);
        subCategoryRepository.save(safetySubCategory9);
        subCategoryRepository.save(rawMaterialSubCategory);










        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");
        john.setImageURL("/assets/images/john.jpg");

        Employee josacore = new Employee();
        josacore.setFirstName("Josimar");
        josacore.setLastName("Condori");
        josacore.setImageURL("/assets/images/josacore.jpg");

        Employee valentin = new Employee();
        valentin.setFirstName("Valentin");
        valentin.setLastName("Laime");
        valentin.setImageURL("/assets/images/valentin.jpg");

        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);
        Position position2 = new Position();
        position2.setName("ADMINISTRATOR");
        positionRepository.save(position2);
        Position position3 = new Position();
        position3.setName("DIRECTIVES");
        positionRepository.save(position3);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        Calendar contracInitDate = Calendar.getInstance();
        contracInitDate.set(2010, Calendar.JANUARY, 1);
        contract.setInitDate(contracInitDate.getTime());
        contract.setPosition(position);

        Contract contract2 = new Contract();
        contract2.setEmployee(josacore);
        Calendar contracInitDate2 = Calendar.getInstance();
        contracInitDate2.set(2017, Calendar.JANUARY, 1);
        contract2.setInitDate(contracInitDate2.getTime());
        contract2.setPosition(position2);


        Contract contract4 = new Contract();
        contract4.setEmployee(valentin);
        Calendar contracInitDate4 = Calendar.getInstance();
        contracInitDate4.set(2006, Calendar.JANUARY, 1);
        contract4.setInitDate(contracInitDate4.getTime());
        contract4.setPosition(position3);

        john.getContracts().add(contract);
        josacore.getContracts().add(contract2);
        valentin.getContracts().add(contract4);

        employeeRepository.save(john);
        employeeRepository.save(josacore);
        employeeRepository.save(valentin);

        contractRepository.save(contract);
        contractRepository.save(contract2);
        contractRepository.save(contract4);









        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setImageURL("/assets/images/helmet.jpg");
        helmet.setPrice(120.99);
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        Comment comment1 = new Comment();
        comment1.setComment("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown");
        comment1.setItem(itemRepository.findById(helmet.getId()));
        comment1.setPublicDate(new Date());
        comment1.setEmployee(valentin);

        Comment comment2 = new Comment();
        comment2.setComment("Ssetting industry.standard dummy text ever since the 1500s, when an unknown");
        comment2.setItem(itemRepository.findById(helmet.getId()));
        comment2.setPublicDate(new Date());
        comment2.setEmployee(employeeRepository.findById(valentin.getId()));

        Comment comment3 = new Comment();
        comment3.setComment("Ssetting industry.standard dummy text ever sifxccvcxv  xcvxcv nce the 1500s, when an unknown");
        comment3.setItem(itemRepository.findById(helmet.getId()));
        comment3.setPublicDate(new Date());
        comment3.setEmployee(josacore);

        Comment comment4 = new Comment();
        comment4.setComment("Ssetting industry.st500s, when an Disposable Respirators And Dust Masks are constructed for comfortable and dependable use during sanding, grinding, sweeping, demolition or low level gaseous operations. ");
        comment4.setItem(itemRepository.findById(helmet.getId()));
        comment4.setPublicDate(new Date());
        comment4.setEmployee(josacore);

        itemRepository.findById(helmet.getId()).getComments().add(comment1);
        itemRepository.findById(helmet.getId()).getComments().add(comment2);
        itemRepository.findById(helmet.getId()).getComments().add(comment3);
        itemRepository.findById(helmet.getId()).getComments().add(comment4);

        itemRepository.save(itemRepository.findById(helmet.getId()));







        Item helmet2 = new Item();
        helmet2.setCode("GLV");
        helmet2.setName("GLOVES");
        helmet2.setImageURL("/assets/images/gloves.jpg");
        helmet2.setPrice(60.99);
        helmet2.setSubCategory(safetySubCategory);

        itemRepository.save(helmet2);

        Comment comment5 = new Comment();
        comment5.setComment("Disposable Respirators And Dust Masks are constructed for comfortable and dependable use during sanding, grinding, w level gaseous operations. ");
        comment5.setItem(itemRepository.findById(helmet2.getId()));
        comment5.setPublicDate(new Date());
        comment5.setEmployee(valentin);

        Comment comment6 = new Comment();
        comment6.setComment("Ssetting industry.st500s, when an unknown");
        comment6.setItem(itemRepository.findById(helmet2.getId()));
        comment6.setPublicDate(new Date());
        comment6.setEmployee(josacore);

        itemRepository.findById(helmet2.getId()).getComments().add(comment5);
        itemRepository.findById(helmet2.getId()).getComments().add(comment6);

        itemRepository.save(itemRepository.findById(helmet2.getId()));





        Item helmet3 = new Item();
        helmet3.setCode("GLS");
        helmet3.setName("GLASSES");
        helmet3.setImageURL("/assets/images/glasses.jpg");
        helmet3.setPrice(20.99);
        helmet3.setSubCategory(safetySubCategory);

        itemRepository.save(helmet3);

        Comment comment7 = new Comment();
        comment7.setComment("Disposable Respirators And Dust Masks are constructed for comfortable and dependable use during sanding, grinding, sweeping, demolition or low level gaseous operations. ");
        comment7.setItem(itemRepository.findById(helmet3.getId()));
        comment7.setPublicDate(new Date());
        comment7.setEmployee(employeeRepository.findById(valentin.getId()));

        Comment comment8 = new Comment();
        comment8.setComment("Disposable Respirators And Dust Masks are constructed for comfortable and dependable use during sanding, grinding, sweeping, demolition or low level gaseous operations. ");
        comment8.setItem(itemRepository.findById(helmet3.getId()));
        comment8.setPublicDate(new Date());
        comment8.setEmployee(employeeRepository.findById(josacore.getId()));

        itemRepository.findById(helmet3.getId()).getComments().add(comment7);
        itemRepository.findById(helmet3.getId()).getComments().add(comment8);

        itemRepository.save(itemRepository.findById(helmet3.getId()));







        Item helmet4 = new Item();
        helmet4.setCode("AUD");
        helmet4.setName("AUDIO");
        helmet4.setImageURL("/assets/images/audio.jpg");
        helmet4.setPrice(40.99);
        helmet4.setSubCategory(safetySubCategory);

        itemRepository.save(helmet4);

        Comment comment9 = new Comment();
        comment9.setComment("Ssetting industry.st500s, when an unknown");
        comment9.setItem(itemRepository.findById(helmet4.getId()));
        comment9.setPublicDate(new Date());
        comment9.setEmployee(john);

        itemRepository.findById(helmet4.getId()).getComments().add(comment9);

        itemRepository.save(itemRepository.findById(helmet4.getId()));






        Item helmet5 = new Item();
        helmet5.setCode("EXTY");
        helmet5.setName("EXTINTOR");
        helmet5.setImageURL("/assets/images/extintor.jpg");
        helmet5.setPrice(500.99);
        helmet5.setSubCategory(safetySubCategory5);

        itemRepository.save(helmet5);

        Comment comment10 = new Comment();
        comment10.setComment("Disposable Respirators And Dust Masks are constructed for comfortable and dependable use during sanding, grinding, sweeping, demolition or low level gaseous operations. ");
        comment10.setItem(itemRepository.findById(helmet5.getId()));
        comment10.setPublicDate(new Date());
        comment10.setEmployee(josacore);

        Comment commet22 = comment9;
        commet22.setItem(itemRepository.findById(helmet5.getId()));
        Comment commet32 = comment8;
        commet32.setItem(itemRepository.findById(helmet5.getId()));

        itemRepository.findById(helmet5.getId()).getComments().add(comment10);
        itemRepository.findById(helmet5.getId()).getComments().add(commet22);
        itemRepository.findById(helmet5.getId()).getComments().add(commet32);


        itemRepository.save(itemRepository.findById(helmet5.getId()));




        Item helmet6 = new Item();
        helmet6.setCode("MSK");
        helmet6.setName("MASK");
        helmet6.setImageURL("/assets/images/mask.jpg");
        helmet6.setPrice(100.99);
        helmet6.setSubCategory(safetySubCategory);

        itemRepository.save(helmet6);

        Comment commet42 = comment7;
        commet42.setItem(itemRepository.findById(helmet6.getId()));
        Comment commet52 = comment6;
        commet52.setItem(itemRepository.findById(helmet6.getId()));
        Comment commet62 = comment5;
        commet62.setItem(itemRepository.findById(helmet6.getId()));
        Comment commet72 = comment4;
        commet72.setItem(itemRepository.findById(helmet6.getId()));
        Comment commet82 = comment3;
        commet82.setItem(itemRepository.findById(helmet6.getId()));
        Comment commet92 = comment2;
        commet92.setItem(itemRepository.findById(helmet6.getId()));
        Comment commet102 = comment1;
        commet102.setItem(itemRepository.findById(helmet6.getId()));

        itemRepository.findById(helmet6.getId()).getComments().add(commet42);
        itemRepository.findById(helmet6.getId()).getComments().add(commet52);
        itemRepository.findById(helmet6.getId()).getComments().add(commet62);
        itemRepository.findById(helmet6.getId()).getComments().add(commet72);
        itemRepository.findById(helmet6.getId()).getComments().add(commet82);
        itemRepository.findById(helmet6.getId()).getComments().add(commet92);
        itemRepository.findById(helmet6.getId()).getComments().add(commet102);


        /*
        commentRepository.save(commet42);
        commentRepository.save(commet52);
        commentRepository.save(commet62);

        commentRepository.save(commet72);
        commentRepository.save(commet82);
        commentRepository.save(commet92);

        commentRepository.save(commet102);
        */

        itemRepository.save(itemRepository.findById(helmet6.getId()));

        try { Comment c = commentRepository.save(comment1); }catch (Exception e){}
        try { Comment c1 = commentRepository.save(comment2);}catch (Exception e){}
            try { Comment c2 = commentRepository.save(comment3);}catch (Exception e){}
                try { Comment c3 = commentRepository.save(comment4);}catch (Exception e){}
                    try { Comment c4 = commentRepository.save(comment5);}catch (Exception e){}
                        try { Comment c5 = commentRepository.save(comment6);}catch (Exception e){}
                            try { Comment c6 = commentRepository.save(comment7);}catch (Exception e){}
        try { Comment c7 = commentRepository.save(comment8);}catch (Exception e){}
            try { Comment c8 = commentRepository.save(comment9);}catch (Exception e){}
                try { Comment c9 = commentRepository.save(comment10);}catch (Exception e){}
                    try { Comment c11 = commentRepository.save(commet22);}catch (Exception e){}
                        try { Comment c12 = commentRepository.save(commet32);}catch (Exception e){}
                            try { Comment c13 = commentRepository.save(commet42);}catch (Exception e){}
                                try { Comment c14 = commentRepository.save(commet52);}catch (Exception e){}
                                    try { Comment c15 = commentRepository.save(commet62);}catch (Exception e){}
        try { Comment c16 = commentRepository.save(commet72);}catch (Exception e){}
            try { Comment c17 = commentRepository.save(commet82);}catch (Exception e){}
                try { Comment c18 = commentRepository.save(commet92);}catch (Exception e){}
                    try { Comment c10 = commentRepository.save(commet102);}catch (Exception e){}

    }
}
