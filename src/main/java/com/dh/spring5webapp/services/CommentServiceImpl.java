/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Comment;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.repositories.CommentRepository;
import com.dh.spring5webapp.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Set<Comment> getComments() {

        Set<Comment> items = new HashSet<>();

        commentRepository.findAll().iterator().forEachRemaining(items::add);
        return items;
    }

    @Override
    public Comment findById(Long id) {
        Optional<Comment> itemOptional = commentRepository.findById(id);
        if (!itemOptional.isPresent()) {
            throw new RuntimeException("ItemNotFound");
        }
        return itemOptional.get();
    }

    @Override
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }
}
