/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Contract;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.repositories.ContractRepository;
import com.dh.spring5webapp.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractRepository contractRepository;

    @Override
    public Set<Contract> getContracts() {

        Set<Contract> contracts = new HashSet<>();

        contractRepository.findAll().iterator().forEachRemaining(contracts::add);
        return contracts;
    }

    @Override
    public Contract findById(Long id) {
        Optional<Contract> contractOptional = contractRepository.findById(id);
        if (!contractOptional.isPresent()) {
            throw new RuntimeException("ItemNotFound");
        }
        return contractOptional.get();
    }

    @Override
    public Contract save(Contract contract) {
        return contractRepository.save(contract);
    }
}
