/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.Employee;

import java.util.Set;

public interface EmployeeService {

    Set<Employee> getEmployees();
    Employee findById(Long id);

    Employee save(Employee employee);

    void delete(Long id);
}
