/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.Position;

import java.util.Set;

public interface PositionService {

    Set<Position> getPositions();
    Position findById(Long id);
    Position save(Position posistion);
}
