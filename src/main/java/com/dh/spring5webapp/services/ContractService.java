/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Contract;
import com.dh.spring5webapp.model.Item;

import java.util.Set;

public interface ContractService {
    Set<Contract> getContracts();

    Contract findById(Long id);

    Contract save(Contract contract);
}
