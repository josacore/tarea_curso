/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.SubCategory;

import java.util.Set;

public interface SubCategoryService {

    Set<SubCategory> getSubCategories();

    SubCategory getSubCategoryById(Long id);

    SubCategory save(SubCategory subcategory);
}
