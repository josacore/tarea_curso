/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.Comment;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

public interface CommentService {

    Set<Comment> getComments();
    Comment findById(Long id);
    @Transactional
    Comment save(Comment comment);
}
