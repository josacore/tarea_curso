/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.model.Position;
import com.dh.spring5webapp.repositories.ItemRepository;
import com.dh.spring5webapp.repositories.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PositionServiceImpl implements PositionService {

    @Autowired
    private PositionRepository positionRepository;

    @Override
    public Set<Position> getPositions() {

        Set<Position> items = new HashSet<>();

        positionRepository.findAll().iterator().forEachRemaining(items::add);
        return items;
    }

    @Override
    public Position findById(Long id) {
        Optional<Position> itemOptional = positionRepository.findById(id);
        if (!itemOptional.isPresent()) {
            throw new RuntimeException("ItemNotFound");
        }
        return itemOptional.get();
    }

    @Override
    public Position save(Position posistion) {
        return positionRepository.save(posistion);
    }
}
