/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.model.SubCategory;
import com.dh.spring5webapp.repositories.ItemRepository;
import com.dh.spring5webapp.repositories.SubCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Override
    public Set<SubCategory> getSubCategories() {

        Set<SubCategory> items = new HashSet<>();

        subCategoryRepository.findAll().iterator().forEachRemaining(items::add);
        return items;
    }

    @Override
    public SubCategory getSubCategoryById(Long id) {
        Optional<SubCategory> itemOptional = subCategoryRepository.findById(id);
        if (!itemOptional.isPresent()) {
            throw new RuntimeException("ItemNotFound");
        }
        return itemOptional.get();
    }

    @Override
    public SubCategory save(SubCategory employee) {
        return subCategoryRepository.save(employee);
    }
}
