/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.dh.spring5webapp.services.EmployeeService;
import org.springframework.stereotype.Controller;

import com.dh.spring5webapp.command.EmployeeCommand;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.repositories.EmployeeRepository;

@Path("/employees")
@Produces("application/json")
@Controller
public class EmployeeController {
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GET
    public Response getEmployees() {
        List<EmployeeCommand> employeeList = new ArrayList<>();
        employeeService.getEmployees().iterator().forEachRemaining(employee -> {

            employeeList.add(new EmployeeCommand(employee));
        });
        return Response.ok(employeeList).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")

                .build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployeeById(@PathParam("id") long id) {
        Employee employee = employeeService.findById(id);
        return Response.ok(new EmployeeCommand(employee)).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")

                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public EmployeeCommand addEmployee(EmployeeCommand employeeCommand) {
        Employee employee = employeeService.save(employeeCommand.toEmployee());
        return new EmployeeCommand(employee);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public EmployeeCommand updateEmployee(EmployeeCommand employeeCommand) {
        Employee employee = employeeService.save(employeeCommand.toEmployee());
        return new EmployeeCommand(employee);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteEmployee(@PathParam("id") long id) {
        employeeService.delete(id);

    }
}