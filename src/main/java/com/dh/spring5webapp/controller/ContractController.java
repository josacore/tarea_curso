/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.command.ContractCommand;
import com.dh.spring5webapp.command.EmployeeCommand;
import com.dh.spring5webapp.model.Contract;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.services.ContractService;
import com.dh.spring5webapp.services.ContractServiceImpl;
import com.dh.spring5webapp.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.ContractRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/contracts")
@Produces("application/json")
@Controller
public class ContractController {

    private ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GET
    public Response getContracts() {
        List<ContractCommand> contractList = new ArrayList<>();
        contractService.getContracts().iterator().forEachRemaining(contract -> {
            contractList.add(new ContractCommand(contract));
        });
        return Response.ok(contractList).build();
    }

    @GET
    @Path("/employee/{id_empleado}")
    public Response getContractsByEmployee(@PathParam("id_empleado") long id_empleado) {
        List<ContractCommand> contractList = new ArrayList<>();
        contractService.getContracts().iterator().forEachRemaining(contract -> {
            if(contract.getEmployeeId() == id_empleado)
                contractList.add(new ContractCommand(contract));
        });
        return Response.ok(contractList).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ContractCommand getContractsById(@PathParam("id") long id) {
        Contract contract = contractService.findById(id);
        return new ContractCommand(contract);
    }

}