/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Item;
import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Contract;

import java.util.Optional;

public interface ContractRepository extends CrudRepository<Contract, Long> {
    Optional<Contract> findById(Long id);
}
