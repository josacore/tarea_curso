/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Position;

import java.util.Optional;

public interface PositionRepository extends CrudRepository<Position, Long> {
    Optional<Position> findById(Long id);
}