package com.dh.spring5webapp.command;

import com.dh.spring5webapp.model.Comment;

public class CommentCommand {

    private String comment;
    private String date;
    private Long employeeId;
    private int likes = 0;

    public CommentCommand() {
    }

    public CommentCommand(Comment comment) {
        this.setComment(comment.getComment());
        this.setDate(comment.getPublicDate().toString());
        this.setEmployeeId(comment.getEmployee().getId());
        this.setLikes(comment.getLikes());
    }

    public CommentCommand(String comment, String date, Long employeeId) {
        this.comment = comment;
        this.date = date;
        this.employeeId = employeeId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
