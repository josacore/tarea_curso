/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.command;

import com.dh.spring5webapp.model.Comment;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.model.Item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ItemCommand {

    private String name;
    private String label;
    private String image;
    private String category;
    private String price;
    private Long id;
    private String description;
    private Boolean featured;
    private List<CommentCommand> comments;

    public ItemCommand() {
        comments= new ArrayList<CommentCommand>();
    }

    public ItemCommand(Item item) {
        this.setCategory(item.getSubCategory().getCategory().getName());
        this.setDescription(item.getName());
        this.setFeatured(true);
        this.setId(item.getId());
        this.setName(item.getName());
        this.setLabel(item.getName());
        this.setImage(item.getImageURL());
        this.setPrice(String.valueOf(item.getPrice()));
        this.comments = new ArrayList<CommentCommand>();
        item.getComments().iterator().forEachRemaining(comment -> {
            this.comments.add(new CommentCommand(comment));
        });
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<CommentCommand> getComments() {
        return comments;
    }
    public void setComments(List<CommentCommand> comments) {
        this.comments = comments;
    }
}
