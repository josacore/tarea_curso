/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.command;

import com.dh.spring5webapp.model.Contract;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.model.ModelBase;

import java.util.Date;

public class ContractCommand extends ModelBase {

    private Date initDate;
    private Date endDate;

    public ContractCommand() {
    }

    public ContractCommand(Contract contract) {
        this.initDate = contract.getInitDate();
        this.endDate = contract.getEndDate();

    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
